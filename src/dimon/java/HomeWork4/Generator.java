package dimon.java.HomeWork4;

import dimon.java.HomeWork4.model.Book;

/**
 * Created by Dimon on 31.03.18.
 */
public final class Generator {

    private Generator(){

    }

    public static Book[] generate(){

        Book[] books = new Book[5];

        String[] newBookName = {"Maugli", "Gaydamaki", "Robinson Cruzo", "Alhimik", "Timeless"};
        String[][] newBookAuthor = {{"Rediard Kipling"}, {"Taras Shevchenko", "Ivan Franko", "Lesya Ukrainka"}, {"Daniel Defo", "Jack Sparrow"}, {"Paulo Coelho"}, {"Kerstin Gier" }};
        int[] newBookYear = {1942, 1841, 1719, 1988, 2009};
        String[] newpublishings = {"Ababahalamaga", "Ranok", "Osvita", "New York Times","Koleso"};

        for (int i = 0; i < books.length; i++) {

            books[i] = new Book(newBookName[i],newBookAuthor[i], newBookYear[i],newpublishings[i]);
        }
        return books;
    }


}
