package dimon.java.HomeWork4;

import dimon.java.HomeWork4.model.Book;

/**
 * Created by Dimon on 31.03.18.
 */
public class Library {
    Book[] books;

    public Library(Book[] books) {
        this.books = books;
    }

    public void printAll(){
        for (Book book1 : books) {
            book1.print();
        }
    }

    public void printOlderBooks(){
        int enteredYear = 1995;
        for (Book book : books) {
            if (book.isOlderThan(1995)) {
                System.out.println("\nFounded book: ");
                book.print();
            }
        }

    }
    public void findAuthor(String foundedBook){

        boolean bookIsFinded = false;
        for (Book book : books) {
            boolean ifIsContains = false;
            for (String bookAuthor : book.bookAuthors) {
                if (bookAuthor.equalsIgnoreCase(foundedBook)) {
                    System.out.println("\nFounded book: \n-");
                    System.out.println(book.bookName + "\n-");
                    bookIsFinded=  true;
                }
            }

        }
        if(!bookIsFinded){
            System.out.println("Book  does not founded");
        }
    }

    public void findAuthor(String fB1, String fB2){
        boolean bookIsFinded = false;
        for (Book book : books) {
            boolean firstAuthor = false;
            boolean secondAuthor = false;
            for (String bookAuthor : book.bookAuthors) {
                if(bookAuthor.equalsIgnoreCase(fB1)){
                    firstAuthor = true;
                }
                if(bookAuthor.equalsIgnoreCase(fB2)){
                    secondAuthor = true;
                }
            }
            if(firstAuthor&&secondAuthor) {
                book.print();
                bookIsFinded = true;
            }
        }
        if(!bookIsFinded) {
            System.out.println("Does not finded");}
    }

    public void wheremoreThreeAndMore(){
        boolean isFinded = false;
        for (Book book : books) {
            if(book.bookAuthors.length>=3){
                book.print();
                isFinded =true;
            }
        }
        if(!isFinded){
            System.out.println("Does not finded");
        }
    }
}
