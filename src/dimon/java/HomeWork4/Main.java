package dimon.java.HomeWork4;

import dimon.java.HomeWork4.model.Book;

public class Main {

    public static void main(String[] args) {

        Library library = new Library(Generator.generate());

        library.printAll();

        String foundedBook = "Taras Shevchenko";
        library.findAuthor(foundedBook);

        library.printOlderBooks();

        System.out.println("\nBooks with yours authors;");
        library.findAuthor("Taras Shevchenko", "Ivan Franko");

        System.out.println("Books that have three and more authors:");
        library.wheremoreThreeAndMore();

    }
}
