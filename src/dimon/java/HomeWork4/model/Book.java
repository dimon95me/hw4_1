package dimon.java.HomeWork4.model;

/**
 * Created by Dimon on 28.03.18.
 */
public class Book {
    public String bookName;
    public String[] bookAuthors;
    public int bookYear;
    public String publishingHouse;

    public Book(){

    }


    public Book(String bookName, String[] bookAuthors, int bookYear, String publishingHouse) {
        this.bookName = bookName;
        this.bookAuthors = bookAuthors;
        this.bookYear = bookYear;
        this.publishingHouse = publishingHouse;
    }
    public void print(){
        System.out.println("--------------");
        System.out.println(bookName);
        System.out.println("Authors:");
        for (String bookAuthor : bookAuthors) {
            System.out.println("\t"+bookAuthor);
        }
        System.out.println(bookYear);
        System.out.println(publishingHouse);
    }

    public boolean isOlderThan(int year){
        return this.bookYear < year;
    }

    public Book isOlderThan(Book book){
        return bookYear>book.bookYear ? this : book;
//        if(bookYear>book.bookYear){
//            return this;
//        } else {
//            return book;
//        }
    }

    public boolean hasAuthor(String nameAuthor, int i) {
        return nameAuthor.equalsIgnoreCase(this.bookAuthors[i]);
    }


}
